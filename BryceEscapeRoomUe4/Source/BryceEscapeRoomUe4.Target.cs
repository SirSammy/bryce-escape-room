// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class BryceEscapeRoomUe4Target : TargetRules
{
	public BryceEscapeRoomUe4Target(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;

		ExtraModuleNames.AddRange( new string[] { "BryceEscapeRoomUe4" } );
	}
}
