#include "Grabber.h"
#include "BryceEscapeRoomUe4.h"
#include "Engine/World.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "GameFramework/PlayerController.h"     
#include "DrawDebugHelpers.h"  
#define OUT
/// Sets default values for this component's properties
UGrabber::UGrabber()
{
	/// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	/// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	/// ...
}
/// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("Grabber repoting for duty!"));
	/// Look for attached Phisics Handle	
	PhysicsHandle = GetOwner()->FindComponentByClass < UPhysicsHandleComponent>();
}


/// Called every frame 
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// get player view point this tick
	FVector  PlayerViewPointLocation;
	FRotator PlayerViewPointRotaion; 
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT PlayerViewPointLocation,
		OUT PlayerViewPointRotaion
	);
	////log out to test 
	//UE_LOG(LogTemp, Warning, TEXT("Location: %s, Position: %s"),
	//	PlayerViewPointLocation.ToString(),
	//	PlayerViewPointRotaion.ToString()
	//);
	FVector Arm;
	Arm.X = PlayerViewPointLocation.X + 10;
	Arm.Y = PlayerViewPointLocation.Y;
	Arm.Z = PlayerViewPointLocation.Z - 30.f;
	FVector LineTraceEnd = PlayerViewPointLocation + PlayerViewPointRotaion.Vector() * Reach; 

	DrawDebugLine(
		GetWorld(),
		Arm,
		LineTraceEnd,
		FColor(0, 0, 255),
		false,
		0.001f,
		0.f,
		1.f
	);
	///set up query param
	FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());
	///ray cast out to reach distance
	FHitResult Hit;

	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		PlayerViewPointLocation,
		LineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParameters
	);
	/// see what we hit
	AActor* ActorHit = Hit.GetActor();
	if(ActorHit)
	{
		UE_LOG(LogTemp, Warning, TEXT("POKEd Line trace hit: %s"), *(ActorHit->GetName()));
	}
}

