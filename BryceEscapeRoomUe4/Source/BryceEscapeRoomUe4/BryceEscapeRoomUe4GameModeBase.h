// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BryceEscapeRoomUe4GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class BRYCEESCAPEROOMUE4_API ABryceEscapeRoomUe4GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
