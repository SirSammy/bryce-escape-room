// Fill out your copyright notice in the Description page of Project Settings.

using UnrealBuildTool;
using System.Collections.Generic;

public class BryceEscapeRoomUe4EditorTarget : TargetRules
{
	public BryceEscapeRoomUe4EditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;

		ExtraModuleNames.AddRange( new string[] { "BryceEscapeRoomUe4" } );
	}
}
